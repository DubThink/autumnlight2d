#pragma once
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include "Sprite.h"

class Entity {
public:
	float x1=100;
	float y1=100;
	float testRadius=0;
	int filter;
	bool tmp_disabled=false;
	void init(float x, float y, float radius, unsigned short idFilter) {
		x1 = x;
		y1 = y;
		testRadius = radius;
		filter = idFilter;
	}
	bool test(float x, float y, unsigned short id) {
		
		//printf("%f %f %f %f %f\n", x, x1, y, y1, testRadius);;;;
		bool trigger = (x1 - x)*(x1 - x) + (y1 - y)*(y1 - y) < testRadius*testRadius;
		//printf("%f %f %f %f %f [[%d || %d]]\n", x, x1, y, y1, testRadius,trigger,tmp_disabled);
		if (trigger) {
			if (!tmp_disabled)return true;
		}
		else {
			if (tmp_disabled)tmp_disabled = false;
		}
		return false;
		//(filter | id)
	}
};

class DialogueEntity : public Entity {
public:
	std::vector<std::string> lines;
	Sprite characterSprite;
	void dbgDraw() {
		characterSprite.display(x1, y1);
	};
	bool hasSound = false;
	DialogueEntity() {};
	DialogueEntity(std::string dialogueFile, std::string spriteFile) {
		DialogueEntity(dialogueFile, spriteFile, 1);
	}
	DialogueEntity(std::string dialogueFile, std::string spriteFile, int spriteParts) {
		std::string line;
		std::fstream myfile(dialogueFile);
		if (myfile.is_open()) {
			while (getline(myfile, line)) {
				lines.push_back(line);
			}
			myfile.close();
		}
		else {
			std::cout << "Unable to open dialogue file. Aborting. :"<<dialogueFile << std::endl;
		}
		printf("Lines loaded:%d\n", lines.size());
		characterSprite = Sprite(0.5f,0.5f, spriteParts,spriteFile.c_str());
	}
};