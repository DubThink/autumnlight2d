#ifndef GAME_H
#define GAME_H

#include "Level.h"
#include "Player.h"
#include "Enemy.h"
#include <SFML/Audio.hpp>
#include <map>

const float box2d_to_world = 0.5f; //World units in a Box2D unit (meter)

class Game {
public:
	enum State{OPEN,DIALOGUE};
	int currentLevel = 0;
	Level* levels;
	b2World *physWorld;
	int32 velocityIterations = 8;
	int32 positionIterations = 3;
	State state=State::OPEN;
	Player you;
	Enemy boogle;

	Game();

	void init();
	void initPhysics();
	void initAudio();
	void loadData();

	void update(const double &deltaTime);
	void drawStuff();
	void drawOverlay();
private:
	int tick = 0;

	void setupDialog();
	class SoundByte {
	public:
		SoundByte() {}
		void init(std::string fileName) {
			buffer.loadFromFile("data/sound/"+fileName);
			s.setBuffer(buffer);
		}
		sf::Sound s;
	private:
		sf::SoundBuffer buffer;
	};

	std::map<std::string,SoundByte> sounds;
	SoundByte sb;
	void loadSound(std::string fileName, std::string name);
};

bool getKey(const int &i);

#endif