#ifndef SPRITE_H
#define SPRITE_H

#include <GL/glew.h>

#include <string>
#include <glm/glm.hpp>

#include "uMesh.h"

class Sprite {
public:
	float w = 0.f;
	float h = 0.f;
	float tex_w = 0.f;
	float tex_h = 0.f;
	unsigned int divisions = 1;
	uMesh mesh;
	GLuint tex;
	Sprite();
	Sprite(const float &w, const float &h);
	Sprite(const float &w, const float &h, const unsigned int& div, const char* floc);
	~Sprite();
	bool hasTex = false;
	void genMesh();
	void display(const float &x, const float &y);
	void displayi(const float &x, const float &y, const int &texnum);
	void displayCentered(const float &x, const float &y);
	void displayCenteredi(const float &x, const float &y, int texnum);
	void display(const float &x, const float &y, const GLuint &tex);
};

#endif