#ifndef LEVEL_H
#define LEVEL_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <Box2D/Box2D.h>

#include <string>
#include <vector>

#include "Sprite.h"

class Level {
public:
	Sprite drawSprite;
	GLuint* bkg_img; //1D texture array
	int** bkg_id; //2D texture index Array
	GLuint* frg_img; //1D texture array f
	int** frg_id; //2D texture index Array f

	std::vector<b2BodyDef> staticGeomDef;
	std::vector<b2Body*> staticGeom; //1D body array

	int level_width = 0;
	int level_height = 0;
	Level();
	Level(b2World &physWorld, std::string level_path);
	void drawBKG(const float &playerx, const float &playery);
	void drawFRG(const float &playerx, const float &playery);
	void loadLevel(b2World &physWorld, std::string level_path);
	float tileSize = 0.f;
};

int countFiles(const char* path);
std::vector<std::string> split(const std::string &s, char delim);

#endif