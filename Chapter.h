#pragma once

class Chapter {
	Chapter(double time) {
		startTime = time;
		init();
	}
	void update(float playerX, float playerY, bool frozen, double time) {
		tick++;
		levelTime = time - startTime;
		_update(playerX, playerY, frozen, time);
	}
private:
	float levelTime = 0;
	float startTime;
	int tick = 0;
	virtual void init() = 0;
	virtual void _update(float playerX, float playerY, bool frozen, double time) = 0;
};