#include "Sprite.h"
#include "GLutil.h"
#include "Tesseract.h"
#include <iostream>
#include <GLFW/glfw3.h>
Sprite::Sprite() {}

Sprite::Sprite(const float &width, const float &height) {
	tex = uLoadImage("data/pink.png");
	hasTex = true;
	tex_w = ILW_SECRET;
	tex_h = ILH_SECRET;
	w = width;
	h = height;
	genMesh();
}

Sprite::Sprite(const float &width, const float &height, const unsigned int& div, const char* floc) {
	tex = uLoadImage(floc);
	hasTex = true;
	tex_w = ILW_SECRET;
	tex_h = ILH_SECRET;
	w = width;
	h = height;
	divisions = div;
	genMesh();
}

void Sprite::genMesh() {
	mesh = uMesh();
	float s = divisions;
	s = 1.f / s;
	mesh.addVert(0.f, 0.f, 0.f); mesh.addUV(0.f, 1.f - s, 0.f);
	mesh.addVert(w, 0.f, 0.f);   mesh.addUV(s, 1.f - s, 0.f);
	mesh.addVert(w, h, 0.f);     mesh.addUV(s, 1.f - 0.f, 0.f);
	mesh.addVert(0.f, h, 0.f);   mesh.addUV(0.f, 1.f - 0.f, 0.f);
	mesh.addFace(0, 1, 2);
	mesh.addFace(0, 3, 2);
	mesh.build();
}

void Sprite::display(const float &x, const float &y) {
	sys_translate(x, y);
	glBindTexture(GL_TEXTURE_2D, tex);
	mesh.display();
	sys_translate(-x, -y);
}

void Sprite::displayCentered(const float &x, const float &y) {
	sys_translate(x - w/2.f, y - h/2.f);
	glBindTexture(GL_TEXTURE_2D, tex);
	mesh.display();
	sys_translate(-x + w/2.f, -y + h/2.f);
}

void Sprite::displayi(const float &x, const float &y, const int &texnum) {
	sys_translate(x, y);
	tex_translate(static_cast<float>(texnum % divisions) / static_cast<float>(divisions), -static_cast<float>(texnum / divisions) / static_cast<float>(divisions));
	glBindTexture(GL_TEXTURE_2D, tex);
	mesh.display();
	tex_identity();
	sys_translate(-x, -y);
}

void Sprite::displayCenteredi(const float &x, const float &y, int texnum) {
	sys_translate(x - w / 2.f, y - h / 2.f);
	tex_translate(static_cast<float>(texnum % divisions) / static_cast<float>(divisions), -static_cast<float>(texnum / divisions) / static_cast<float>(divisions));
	glBindTexture(GL_TEXTURE_2D, tex);
	mesh.display();
	tex_identity();
	sys_translate(-x + w / 2.f, -y + h / 2.f);
}

void Sprite::display(const float &x, const float &y, const GLuint &texture) {
	sys_translate(x, y);
	glBindTexture(GL_TEXTURE_2D, texture);
	mesh.display();
	sys_translate(-x, -y);
}

Sprite::~Sprite() {
	//TODO - Delete the mesh or somethin'
	//if(hasTex) glDeleteTextures(1, &tex);
}