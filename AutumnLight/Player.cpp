#include "Player.h"

#include <iostream>

#include "Game.h"

Player::Player() {

}
int orientation = 0;
void Player::init(b2World &physWorld, const float &x, const float &y) {
	pos = b2Vec2(x, y);
	b2BodyDef myBodyDef;
	myBodyDef.type = b2_dynamicBody; //this will be a dynamic body
	myBodyDef.position.Set(0, 0); //set the starting position
	myBodyDef.angle = 0; //set the starting angle
	
	body = physWorld.CreateBody(&myBodyDef);

	b2PolygonShape boxShape;
	boxShape.SetAsBox(1, 1);

	b2CircleShape circle;
	circle.m_radius = 0.4f;

	b2FixtureDef ballFixtureDef;
	ballFixtureDef.shape = &circle;
	ballFixtureDef.density = 1.0f;
	ballFixtureDef.friction = 0.25;
	ballFixtureDef.restitution = 0.05;
	body->CreateFixture(&ballFixtureDef);

	flashy = Sprite(0.4f, 0.4f, 2, "data/charicter1.png");
}

void Player::update() {
	b2Vec2 dir=b2Vec2(0,0);
	if (getKey(GLFW_KEY_UP)) { orientation = 2; dir += b2Vec2(0.f, 1.f); }
	if (getKey(GLFW_KEY_DOWN)) { orientation = 0; dir += b2Vec2(0.f, -1.f); }
	if (getKey(GLFW_KEY_RIGHT)) { orientation = 1; dir += b2Vec2(1.f, 0.f); }
	if (getKey(GLFW_KEY_LEFT)) { orientation = 3; dir += b2Vec2(-1.f, 0.f); }
	dir.Normalize();
	dir *= strength;
	body->ApplyForce(dir, body->GetWorldCenter(), true);
	pos = body->GetPosition();
	angle = body->GetAngle();

	b2Vec2 currentForwardNormal = body->GetLinearVelocity();
	float currentForwardSpeed = currentForwardNormal.Normalize();
	float dragForceMagnitude = -0.4f * currentForwardSpeed;
	body->ApplyForce(dragForceMagnitude * currentForwardNormal, body->GetWorldCenter(), true);

	//std::cout << body->GetPosition().x << " " << body->GetPosition().y << " " << body->GetAngle() << std::endl;
}

void Player::display() {
	flashy.displayCenteredi(pos.x*box2d_to_world, pos.y*box2d_to_world,orientation);
}

bool Player::toggleNoClip()
{
	auto filt = body->GetFixtureList()[0].GetFilterData();
	//printf("FILT %d\n", filt.maskBits==);
	filt.maskBits = 0xFFFF - filt.maskBits;
	body->GetFixtureList()[0].SetFilterData(filt);
	return filt.maskBits==0 ;
}
