#include "Level.h"
#include "dirent.h"

#include "GLUtil.h"
#include "Game.h"

#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>

Level::Level() {}

Level::Level(b2World &physWorld, std::string level_path) {
	loadLevel(physWorld, level_path);
}
void Level::loadLevel(b2World &physWorld, std::string level_path) {

	// COUNT NUMBER OF FILES IN BKG/FRG FOLDERS
	std::cout << "Loading level from " << level_path << "..." << std::endl;
	int bkgNum = countFiles((level_path + "/bkg").c_str());
	std::cout << bkgNum << " BKG tiles found..." << std::endl;

	int frgNum = countFiles((level_path + "/frg").c_str());
	std::cout << frgNum << " FRG tiles found..." << std::endl;

	// LOAD BKG/FRG FOLDERS INTO TILE ARRAYS
	bkg_img = new GLuint[bkgNum];
	for (int i = 0; i < bkgNum; i++) {
		std::string floc = level_path + "/bkg/" + std::to_string(i) + ".png";
		bkg_img[i] = uLoadImage(floc.c_str());
	}
	std::cout << "Tiles loaded." << std::endl;

	frg_img = new GLuint[frgNum];
	for (int i = 0; i < frgNum; i++) {
		std::string floc = level_path + "/frg/" + std::to_string(i) + ".png";
		frg_img[i] = uLoadImage(floc.c_str());
	}
	std::cout << "Tiles loaded." << std::endl;

	// READ TILE PLACEMENT FILE INTO ARRAY

	std::string line;
	std::fstream myfile(level_path + "/bkg_placement.txt");
	std::vector<std::string> bkg_txt;
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			bkg_txt.push_back(line);
		}
		myfile.close();
	}
	else {
		std::cout << "Unable to open bkg_placement file. Aborting." << std::endl;
		return;
	}

	std::string line2;
	std::fstream myfile2(level_path + "/frg_placement.txt");
	std::vector<std::string> frg_txt;
	if (myfile2.is_open()) {
		while (getline(myfile2, line2)) {
			frg_txt.push_back(line2);
		}
		myfile2.close();
	}
	else {
		std::cout << "Unable to open frg_placement file. Aborting." << std::endl;
		return;
	}

	// PROCESS TILE PLACEMENT DATA STORED
	int w = split(bkg_txt[0], ' ').size();
	int h = bkg_txt.size();
	level_width = w;
	level_height = h;
	std::cout << "Map size: " << w << "x" << h << std::endl;
	bkg_id = new int*[w];
	for (int i = 0; i < w; i++) {
		bkg_id[i] = new int[h];
	}
	for (int y = 0; y < h; y++) {
		std::vector<std::string> elems = split(bkg_txt[y], ' ');
		for (int x = 0; x < w; x++) {
			bkg_id[x][level_height - y - 1] = std::stoi(elems[x]);
		}
	}

	frg_id = new int*[w];
	for (int i = 0; i < w; i++) {
		frg_id[i] = new int[h];
	}
	for (int y = 0; y < h; y++) {
		std::vector<std::string> elems = split(frg_txt[y], ' ');
		for (int x = 0; x < w; x++) {
			frg_id[x][level_height - y - 1] = std::stoi(elems[x]);
		}
	}
	std::cout << "Tile placement loaded." << std::endl;

	// READ GAME SETTINGS DATA
	myfile = std::fstream(level_path + "/settings.txt");
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			std::vector<std::string> elems = split(line, ' ');
			if (elems[0] == "tile_size") {
				tileSize = std::stof(elems[1]);
			}
		}
		myfile.close();
	}
	else {
		std::cout << "Unable to open level settings file. Aborting." << std::endl;
		return;
	}
	std::cout << "Settings loaded." << std::endl;

	//READ DATA FROM PHYSICS FILE


	std::vector<b2ChainShape*> chains;
	myfile = std::fstream(level_path + "/physics.txt");
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			std::vector<std::string> elems = split(line, ' ');
			//Create chain/chain loop shapes using physics file data
			if (elems[0] == "c" || elems[0] == "cl") {
				b2ChainShape* chain = new b2ChainShape();
				int n = (elems.size() - 1) / 2;
				b2Vec2 *vs = new b2Vec2[n];
				for (int i = 1; i < elems.size() - 1; i += 2) {
					vs[(i - 1) / 2] = b2Vec2(std::stof(elems[i]) / box2d_to_world, std::stof(elems[i + 1]) / box2d_to_world);
				}
				if (elems[0] == "c")
					chain->CreateChain(vs, n);
				else
					chain->CreateLoop(vs, n);
				chains.push_back(chain);
				delete[] vs;
			}
		}
		myfile.close();
	}
	else {
		std::cout << "Unable to open level physics file. Aborting." << std::endl;
		return;
	}

	for (b2ChainShape* s : chains) {
		b2BodyDef bdef;
		bdef.type = b2_staticBody;
		bdef.position.Set(0.0f, 0.0f);
		bdef.allowSleep = false;
		staticGeomDef.push_back(bdef);

		staticGeom.push_back(physWorld.CreateBody(&staticGeomDef[staticGeomDef.size() - 1]));
		staticGeom[staticGeom.size() - 1]->CreateFixture(&*s, 10.0f);
	}

	std::cout << "Physics data loaded." << std::endl;

	// CREATE SPRITE FOR DRAWING TILES
	drawSprite = Sprite(tileSize, tileSize);
	std::cout << "Level loaded." << std::endl;
}

void Level::drawBKG(const float &playerx, const float &playery) {
	int xstart = 0;
	int xstop = level_width;
	int ystart = 0;
	int ystop = level_height;
	for (int x = xstart; x < xstop; x++) {
		for (int y = ystart; y < ystop; y++) {
			drawSprite.display(x*tileSize, y*tileSize, bkg_img[bkg_id[x][y]]);
		}
	}
	//std::cout << (*staticGeom[0]).GetPosition().x << " " << (*staticGeom[0]).GetPosition().y << " " << (*staticGeom[0]).GetAngle() << std::endl << std::endl;
}

void Level::drawFRG(const float &playerx, const float &playery) {
	int xstart = 0;
	int xstop = level_width;
	int ystart = 0;
	int ystop = level_height;
	for (int x = xstart; x < xstop; x++) {
		for (int y = ystart; y < ystop; y++) {
			drawSprite.display(x*tileSize, y*tileSize, frg_img[frg_id[x][y]]);
		}
	}
	//std::cout << (*staticGeom[0]).GetPosition().x << " " << (*staticGeom[0]).GetPosition().y << " " << (*staticGeom[0]).GetAngle() << std::endl << std::endl;
}

int countFiles(const char* path) {
	int i = -2;
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(path)) != NULL) {
		while (readdir(dir))
			i++;
		(void)closedir(dir);
	}
	else {
		std::cerr << "Directory path " << path << " not found!";
	}
	return i;
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}