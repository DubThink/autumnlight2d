#pragma once
#include <glm/glm.hpp>
#include <Box2D/Box2D.h>
#include "Sprite.h"

class Enemy {
private:
	Sprite flashy;
	float time;
public:
	Enemy();
	float strength = .25;
	b2Vec2 pos;
	b2Body* body;
	float32 angle;
	void init(b2World &world, const float &x, const float &y);
	void update();
	void display();
	b2Vec2 aiStep();
};
