
#include "Entity.h"

void Entity::init(float x, float y, float radius, unsigned short idFilter) {
	x1 = x;
	y1 = y;
	testRadius = radius;
	filter = idFilter;
}

bool Entity::test(float x, float y, unsigned short id) {
	//printf( "%f %f %f %f %f\n", x,x1,y,y1, testRadius);
	//float xdiff = (x1 - x)*(x1 - x);
	//float ydiff = (y1 - y)*(y1 - y);
	//printf("%f %f", xdiff, ydiff);
	//return false;
	//printf(" pooooo %f %f\n", 3.24,32.8);
	return ((x1 - x)*(x1 - x) + (y1 - y)*(y1 - y) < testRadius*testRadius);

	return false;
	//(filter | id)
}

DialogueEntity::DialogueEntity(std::string dialogueFile, std::string spriteFile) {
	std::string line;
	std::fstream myfile(dialogueFile);
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			lines.push_back(line);
		}
		myfile.close();
	}
	else {
		std::cout << "Unable to open dialogue file. Aborting." << std::endl;
	}
	characterSprite = Sprite(0.5f, 0.5f, 0, spriteFile.c_str());
}

void DialogueEntity::dbgDraw() {
	characterSprite.display(x1, y1);
}