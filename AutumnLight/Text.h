#ifndef TEXT_H
#define TEXT_H


#include "Sprite.h"

namespace Text {
	void textInit();
	void text(float x, float y, std::string text);
	void text2(float x, float y, std::string text);
}
#endif // !TEXT_H