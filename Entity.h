#pragma once

class Entity {
	float x;
	float y;
	float radius;
	int filter;
	init(float x, float y, float radius, unsigned short filter) {
		x1 = x;
		y1 = y;
		this.radius = radius;
		this.filter = filter;
	}
	bool inline test(float x, float y, unsigned short id) {
		return (filter | id)&&((y1 - y)*(y1 - y) + (x1 - x)*(x1 - x) < radius*radius);
		
	}
}

class DialogueEntity : Entity {
	DialogueEntity(std::vector<std::string>)
}