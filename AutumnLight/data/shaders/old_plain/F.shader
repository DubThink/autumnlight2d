#version 330 core
uniform sampler2D TA;
in vec2 texCoord;

void main(void) {
	gl_FragColor = texture2D(TA, texCoord);
}