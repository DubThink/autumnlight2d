#include "Text.h"
Sprite font;
Sprite font2;

void Text::textInit() {
	font = Sprite(0.125f, 0.125f, 32, "data/SCP.png");
	font2 = Sprite(.1f, .1f, 32, "data/SCPinv.png");
}
void Text::text(float x, float y, std::string text) {
	for (int i = 0; i < text.length(); i++) {
		font.displayi(x + .06*i, y, (int)text[i] - 32);
	}
}
void Text::text2(float x, float y, std::string text) {
	for (int i = 0; i < text.length(); i++) {
		font2.displayi(x + .06*i, y, (int)text[i] - 32);
	}
}