#version 330 core
attribute vec3 uStream0;
attribute vec3 uStream8;
out vec2 texCoord;

uniform mat4 internal_tsf;
uniform mat4 tsf;
uniform mat4 proj;

void main() {
	gl_Position = proj*(tsf*(internal_tsf*vec4(uStream0, 1.0)));
	texCoord = uStream8.st;
}