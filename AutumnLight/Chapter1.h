#pragma once
#include "Chapter.h"
#include "Entity.h"
class Chapter1 :public Chapter {
public:
	using Chapter::Chapter;
	bool checkDialogue(float x, float y);
	virtual void init();
	DialogueEntity currentDialogue;
	void drawStuff();
	void disableDialogueTmp();
private:
	virtual void _update(float playerX, float playerY, bool frozen, double time);
};