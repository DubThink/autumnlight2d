#include "Game.h"


#include <Box2D/Box2D.h>
#include <SFML/Audio.hpp>

#include "Text.h"
#include "Tesseract.h"
#include "Chapter1.h"
#include "Entity.h"
#include "Sprite.h"
#include <iostream>

Chapter1 chapter1 = Chapter1(3);
int dialogueStep;
int dialogueSteps;
int dialogueLetter;
int dialogueLetters;
int hintkey = -1;
Sprite sign,keys;
bool activatedP = false;
bool activated = false;

bool isVowel(char a) {
	return a == 'a' || a == 'e' || a == 'i' || a == 'o' || a == 'u';
}

Game::Game() {
	state = State::OPEN;
}

void Game::init() {
	initAudio();
	initPhysics();
	loadData();
	you.init(*physWorld, 0.f, 0.f);
	//boogle.init(*physWorld, 1.f, 1.f);
	sounds["load1"].s.play();
	sign = Sprite(2, 2, 1, "data/speech.png");
	keys = Sprite(.25, .25, 8, "data/keys.png");
	chapter1.init();
}

void Game::update(const double &deltaTime) {
	tick++;
	activatedP = activated;
	activated = getKey(GLFW_KEY_E);
	hintkey = -1;

	//std::cout << "Running game update \n";
	if (state == State::OPEN) {
		you.update();
		//boogle.update();
		physWorld->Step(deltaTime, velocityIterations, positionIterations);
		//sound.setPitch((1.1+std::sin(tick / 300.0))/2.0);
		//if(tick%50==0)
		bool doDialogue = chapter1.checkDialogue(you.pos.x, you.pos.y);
		//printf("Returned: %d\n", doDialogue);
		if (doDialogue) {
			if (getKey(GLFW_KEY_E) == GLFW_PRESS) {
				setupDialog();
				state = State::DIALOGUE;
			}
			else {
				hintkey = 4;
			}
		}
		
	}
	else {
		//Dialogue
		if (activated && !activatedP) {
			dialogueStep++;
			if (dialogueStep >= dialogueSteps) {
				state = State::OPEN;
				chapter1.disableDialogueTmp();
			}
			else {
				dialogueLetter = 0;
				dialogueLetters = chapter1.currentDialogue.lines[dialogueStep].length();
			}
		}
	}
}


void Game::drawStuff() {
	translate(-you.pos.x*box2d_to_world, -you.pos.y*box2d_to_world);
	levels[currentLevel].drawBKG(0.f, 0.f);
	chapter1.drawStuff();
	you.display();
	boogle.display();
	levels[currentLevel].drawFRG(0.f, 0.f);

}

void Game::drawOverlay() {
	if (state == State::DIALOGUE) {
		sign.display(-1.25, -1);
		if (dialogueSteps > dialogueStep)Text::text2(-1.15, .825, chapter1.currentDialogue.lines[dialogueStep]);
		if (chapter1.currentDialogue.hasSound) {
			if (dialogueLetter < dialogueLetters) {
				if (sounds["oldman1"].s.getStatus() == sounds["oldman1"].s.Stopped)sounds["oldman1"].s.play();
				sounds["oldman1"].s.setLoop(true);
			}
			else sounds["oldman1"].s.setLoop(false);
		}
		if (dialogueLetter < dialogueLetters&&isVowel(chapter1.currentDialogue.lines[dialogueStep][dialogueLetter])) {
			chapter1.currentDialogue.characterSprite.displayi(-1.75, .5, 1);
		}else chapter1.currentDialogue.characterSprite.displayi(-1.75, .5, 0);
		if(tick%3==0)dialogueLetter++;
	}
	if(hintkey>-1)keys.displayi(.1,.1, hintkey);
}
void Game::setupDialog()
{
	dialogueSteps = chapter1.currentDialogue.lines.size();
	dialogueStep = 0;
	dialogueLetter = 0;
	dialogueLetters = chapter1.currentDialogue.lines[dialogueStep].length();
}

void Game::loadSound(std::string fileName, std::string name)
{
	sounds[name] = SoundByte();
	sounds[name].init(fileName);
}

void Game::initPhysics() {
	physWorld = new b2World(b2Vec2(0.f, -0.f));
}

void Game::initAudio() {
	loadSound("load1.wav", "load1");
	loadSound("oldmantalk1.wav", "oldman1");
	//sounds["oldman1"].s.setLoop(true);
}

void Game::loadData() {
	levels = new Level[1];
	levels[0] = Level(*physWorld, "data/game/level0");
}

bool getKey(const int &i) {
	return getKeyGLFW(i);
}

