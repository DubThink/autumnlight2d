#ifndef TESSERACT_H
#define TESSERACT_H

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

void initialize();

void loop();

bool shouldClose();

void killWindow();

/**
Resets the texture offset uniform variable being passed to the draw shader.
*/
void tex_identity();

/**
Translates texture coordinates.
@param x The x-coordinate of the translation.
@param y The y-coordinate of the translation.
*/
void tex_translate(const float &x, const float &y);

/**
Translates the system transformation matrix to a specified point.
@param x The x-coordinate of the translation.
@param y The y-coordinate of the translation.
*/
void sys_translate(const float &x, const float &y);

/**
Translates the main transformation matrix to a specified point.
@param x The x-coordinate of the translation.
@param y The y-coordinate of the translation.
*/
void translate(const float &x, const float &y);

bool getKeyGLFW(const int &i);

glm::vec2 getCursorPos();

bool isKeyPressed(const int &k);

#endif