#version 330 core
uniform sampler2D TA;
in vec2 texCoord;
out vec4 fragColor;
uniform vec2 tex_offset;

void main() {
	fragColor = vec4(texture2D(TA, texCoord + tex_offset));
}