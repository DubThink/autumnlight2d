#include "Enemy.h"

#include <iostream>

#include "Game.h"

Enemy::Enemy() {

}

void Enemy::init(b2World &physWorld, const float &x, const float &y) {
	pos = b2Vec2(x, y);
	b2BodyDef myBodyDef;
	myBodyDef.type = b2_dynamicBody; //this will be a dynamic body
	myBodyDef.position.Set(0, 0); //set the starting position
	myBodyDef.angle = 0; //set the starting angle
	
	body = physWorld.CreateBody(&myBodyDef);

	b2PolygonShape boxShape;
	boxShape.SetAsBox(1, 1);

	b2CircleShape circle;
	circle.m_radius = 0.1f;

	b2FixtureDef ballFixtureDef;
	ballFixtureDef.shape = &circle;
	ballFixtureDef.density = 1.0f;
	ballFixtureDef.friction = 0.25f;
	ballFixtureDef.restitution = 0.25f;
	body->CreateFixture(&ballFixtureDef);

	flashy = Sprite(0.1f, 0.1f, 1, "data/EnemyNumber1.png");
}

void Enemy::update() {
	b2Vec2 dir;
	dir=aiStep();
	dir.Normalize();
	dir *= strength;
	body->ApplyForce(dir, body->GetWorldCenter(), true);
	pos = body->GetPosition();
	angle = body->GetAngle();

	b2Vec2 currentForwardNormal = body->GetLinearVelocity();
	float currentForwardSpeed = currentForwardNormal.Normalize();
	float dragForceMagnitude = -0.1f * currentForwardSpeed;
	body->ApplyForce(dragForceMagnitude * currentForwardNormal, body->GetWorldCenter(), true);

	std::cout << body->GetPosition().x << " " << body->GetPosition().y << " " << body->GetAngle() << std::endl;
}

void Enemy::display() {
	flashy.displayCentered(pos.x*box2d_to_world, pos.y*box2d_to_world);
}

b2Vec2 Enemy::aiStep()
{
	time += 0.15;
	return b2Vec2(std::sin(time),std::cos(time));
}
