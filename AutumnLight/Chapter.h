#pragma once

class Chapter {
public:
	Chapter(double time) {
		startTime = time;
	}
	void update(float playerX, float playerY, bool frozen, double time) {
		tick++;
		levelTime = time - startTime;
		_update(playerX, playerY, frozen, time);
	}
	float levelTime = 0;
	float startTime;
	int tick = 0;
	virtual void init() {};
private:
	virtual void _update(float playerX, float playerY, bool frozen, double time) {};
};