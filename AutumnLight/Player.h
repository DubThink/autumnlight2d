#pragma once

#include <glm/glm.hpp>
#include <Box2D/Box2D.h>
#include "Sprite.h"

class Player {
private:
	Sprite flashy;
public:
	Player();
	float strength = 1.f;
	b2Vec2 pos;
	b2Body* body;
	float32 angle;
	void init(b2World &world, const float &x, const float &y);
	void update();
	void display();
	bool toggleNoClip();
};
