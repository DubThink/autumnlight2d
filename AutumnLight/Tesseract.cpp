
//--------------------------------------------------------------------//

#include <GL/glew.h>  

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/trigonometric.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <IL/ilut.h>

#include <stdio.h>
#include <stdlib.h>

#include <iostream>

#include <vector>

#include <time.h>

#include "GLUtil.h"
#include "FPSCounter.h"
#include "uMesh.h"
#include "Level.h"
#include "Sprite.h"
#include "Game.h"

#include "Text.h"

//--------------------------------------------------------------------//
GLFWwindow* window;

int resSX = 1024;
int resSY = 640;
int prsx = 1024;
int prsy = 640;

FPSCounter FrameSpeedCounter;

GLuint sdPlain;
GLuint sdDraw;

GLuint FBO0[2];
GLuint FBO1[2];
GLuint FBO2[2];
GLuint FBO3[2];
GLuint FBO4[2];

glm::mat4 projMat;
glm::mat4 tsfMat;
glm::mat4 internal_tsfMat;
glm::vec2 tex_offset;

Game world;


bool debugScreen = true;

int deepTick = 0;
//--------------------------------------------------------------------//
void renderStep();
void stencilStep(int i);
void loop();
void renderGL(int mode);
void setUpGL();
bool shouldClose();
void killWindow();
//--------------------------------------------------------------------//

void deleteFBOS() {
	glDeleteFramebuffers(1, &FBO0[0]);
}

void createFBOS() {
	uNewFBO_FCrD(FBO0, resSX, resSY);
}

void remakeFBOS() {
	deleteFBOS();
	createFBOS();
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_F3 && action == GLFW_PRESS)
		debugScreen = !debugScreen;
	if (key == GLFW_KEY_F4 && action == GLFW_PRESS)
		world.you.toggleNoClip();
}

void sys_translate(const float &x, const float &y);
void translate(const float &x, const float &y);

void initialize() {
	fprintf(stdout, "/////CORE INITIALIZATION STARTED/////\n");

	if (!glfwInit()) {
		fprintf(stderr, "Failed to initialize GLFW\n");
		system("PAUSE");
		exit(EXIT_FAILURE);
	}
	else
		fprintf(stdout, "GLFW initialized successfully!\n");

	window = glfwCreateWindow(1280, 720, "AutumnLight", NULL, NULL);
	glfwGetWindowSize(window, &resSX, &resSY);
	if (!window) {
		fprintf(stderr, "Failed to open GLFW window.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	else
		fprintf(stdout, "GLFW window created successfully!.\n");
	glfwMakeContextCurrent(window);

	printf("OpenGL Version: %s\n", glGetString(GL_VERSION));

	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		system("PAUSE");
		exit(EXIT_FAILURE);
	}
	else
		fprintf(stdout, "GLEW initialized successfully!\n");

	ilInit();
	iluInit();
	ilutRenderer(ILUT_OPENGL);
	fprintf(stdout, "DevIL initialized successfully!\n");

	fprintf(stdout, "/////CORE INITIALIZATION COMPLETED/////\n\n");
	//--------------------------------------------------------------------//

	sdPlain = uLoadShader("data/shaders/plain");
	sdDraw = uLoadShader("data/shaders/draw");

	createFBOS();

	srand(time(NULL));

	tsfMat = glm::mat4();
	setUpGL();

	world.init();

	
	Text::textInit();

	glfwSetKeyCallback(window, key_callback);

	std::cout << "Setup completed." << std::endl;
}

void loop() {
	uRenderToFBO(0);
	glfwGetWindowSize(window, &resSX, &resSY);
	if (prsx != resSX || prsy != resSY) {
		prsy = resSY;
		remakeFBOS();
		prsx = resSX;
	}
	FrameSpeedCounter.tick();

	//--------------------------------------------------------------------//

	projMat = glm::mat4();
	projMat = glm::scale(tsfMat, glm::vec3(static_cast<float>(resSY) / static_cast<float>(resSX), 1.f, 1.f));
	uSetUniformMatrix4x4NoSwitch("proj", projMat);

	internal_tsfMat = glm::mat4();
	uSetUniformMatrix4x4NoSwitch("internal_tsf", internal_tsfMat);

	tsfMat = glm::mat4();
	uSetUniformMatrix4x4NoSwitch("tsf", internal_tsfMat);

	tex_offset = glm::vec2();

	uRenderToFBO(FBO0[0]);
	glEnable(GL_DEPTH_TEST);
	glViewport(0, 0, resSX, resSY);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDepthMask(GL_TRUE);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDisable(GL_DEPTH_TEST);
	glUseProgram(sdDraw);

	world.update(FrameSpeedCounter.deltaTime);
	world.drawStuff();
	internal_tsfMat = glm::mat4();
	tsfMat = glm::mat4();
	sys_translate(0.f, 0.f);
	translate(0, 0);
	world.drawOverlay();
	//Text::font.displayi(-0.5, -0.5, 34);
	char buff[100];
	snprintf(buff,sizeof(buff), "X:%.2f Y:%.2f Dialog=%d", world.you.pos.x, world.you.pos.y,world.state==world.DIALOGUE);
	if(debugScreen)Text::text(-1.75, -1,buff);
	uSetUniform2f(sdPlain, "RES", resSX, resSY);
	uFilterFBOToFBO(0, sdPlain, resSX, resSY, 1, FBO0[1]);

	glfwSwapBuffers(window);
	glfwPollEvents();
	if (getKey(GLFW_KEY_F2)) {
		char cmd[100];
		std::cin.getline(cmd,100);
		if (strcmp("noclip", cmd) == 0) {
			printf("TOGGLE NOCLIP\n");
			world.you.toggleNoClip();
		}
	}
	deepTick++;
}

void tex_identity() {
	tex_offset = glm::vec2();
	uSetUniform2fNoSwitch("tex_offset", tex_offset.x, tex_offset.y);
}

void tex_translate(const float &x, const float &y) {
	tex_offset = glm::vec2(tex_offset.x + x, tex_offset.y + y); //sorry glm gotta go fast
	uSetUniform2fNoSwitch("tex_offset", tex_offset.x, tex_offset.y);
}

void sys_translate(const float &x, const float &y) {
	internal_tsfMat = glm::translate(internal_tsfMat, glm::vec3(x, y, 0));
	uSetUniformMatrix4x4NoSwitch("internal_tsf", internal_tsfMat);
}

void translate(const float &x, const float &y) {
	tsfMat = glm::translate(tsfMat, glm::vec3(x, y, 0));
	uSetUniformMatrix4x4NoSwitch("tsf", tsfMat);
}

glm::vec2 getCursorPos() {
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);
	return glm::vec2(xpos, ypos);
}

bool getKeyGLFW(const int &i) {
	return glfwGetKey(window, i);
}

void setUpGL() {
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_TEXTURE0);
	glEnable(GL_TEXTURE1);
	glEnable(GL_TEXTURE2);
	glEnable(GL_TEXTURE3);
	glEnable(GL_TEXTURE4);
	glEnable(GL_TEXTURE5);
	glEnable(GL_TEXTURE6);
	glEnable(GL_TEXTURE7);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glActiveTexture(GL_TEXTURE0);
	//glClearColor(0.1f, 0.1f, 0.1f, 1.f);
}

bool shouldClose() {
	return(glfwWindowShouldClose(window)) != 0;
}

bool isKeyPressed(const int &k) {
	return glfwGetKey(window, k);
}

void killWindow() {
	deleteFBOS();
	glfwDestroyWindow(window);
	glfwTerminate();
}