#pragma once
#include "Chapter.h"
class Chapter1 : Chapter {
private:
	void init();
	void _update(float playerX, float playerY, bool frozen, double time);
};