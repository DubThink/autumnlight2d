#version 330 core

attribute vec3 uStream0; //Positions
attribute vec3 uStream8; //Texture coordinates
uniform mat4 modelViewProjectionMatrix;
out vec2 texCoord;

void main() {
	gl_Position = vec4(uStream0, 1.0);
	texCoord = uStream8.st;
}